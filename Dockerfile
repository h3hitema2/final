# Utilisez une image Apache avec PHP
FROM php:7.4-apache

# Définissez le répertoire de travail dans le conteneur
WORKDIR /var/www/html

# Copiez le code source dans le conteneur
COPY . /var/www/html/

# Installez les dépendances PHP nécessaires
RUN docker-php-ext-install mysqli

# Activez le module rewrite d'Apache pour les URL conviviales
RUN a2enmod rewrite

# Exposez le port 80 pour le serveur Apache
EXPOSE 80

# Commande par défaut pour démarrer Apache lorsque le conteneur est lancé
CMD ["apache2-foreground"]
